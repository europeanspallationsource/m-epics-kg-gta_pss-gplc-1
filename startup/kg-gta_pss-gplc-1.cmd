
# @field IPADDR
# @type STRING
# PLC IP address

# @field RECVTIMEOUT
# @type INTEGER
# PLC->EPICS receive timeout (ms), should be longer than frequency of PLC SND block trigger (REQ input)

# @field REQUIRE_kg-gta_pss-gplc-1_VERSION
# @runtime YES

# @field SAVEFILE_DIR
# @type  STRING
# The directory where autosave should save files

# @field REQUIRE_kg-gta_pss-gplc-1_PATH
# @runtime YES

# S7 port           : 2000
# Input block size  : 112 bytes
# Output block size : 0 bytes
# Endianness        : BigEndian
s7plcConfigure("KG-GTA:PSS-GPLC-1", $(IPADDR), 2000, 112, 0, 1, $(RECVTIMEOUT), 0)

# Modbus port       : 502
drvAsynIPPortConfigure("KG-GTA:PSS-GPLC-1", $(IPADDR):502, 0, 0, 1)

# Link type         : TCP/IP (0)
modbusInterposeConfig("KG-GTA:PSS-GPLC-1", 0, $(RECVTIMEOUT), 0)

# Slave address     : 0
# Function code     : 16 - Write Multiple Registers
# Addressing        : Absolute (-1)
# Data segment      : 2 words
drvModbusAsynConfigure("KG-GTA:PSS-GPLC-1write", "KG-GTA:PSS-GPLC-1", 0, 16, -1, 2, 0, 0, "S7-1500")

# Load plc interface database
dbLoadRecords("kg-gta_pss-gplc-1.db", "PLCNAME=KG-GTA:PSS-GPLC-1, MODVERSION=$(REQUIRE_kg-gta_pss-gplc-1_VERSION), S7_PORT=2000, MODBUS_PORT=502")
