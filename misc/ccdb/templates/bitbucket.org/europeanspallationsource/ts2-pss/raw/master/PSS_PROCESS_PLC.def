############################ TS2 Process PLC System Diagnostics ##############################
############################
#  STATUS BLOCK
############################

set_defaults(ARCHIVE=True)
set_defaults(add_minor_alarm, add_major_alarm, ALARM_IF=False)

define_status_block()

#################################
#Checksums
add_string("Process software checksum", 16,                                   PV_NAME="SW_Checksum",      PV_DESC="Chksum of standard blocks in process PLC")
add_string("Collective f-signature",     8,                                   PV_NAME="FSignature",       PV_DESC="Chksum of safety program in process PLC")

#################################
#Safety PLC time
add_analog("Safety PLC time",            "REAL",                              PV_NAME="Timestamp",        PV_DESC="Time of safety PLC (archive once a day)")

#################################
#System error (should be TRUE if any of the following is in alarm state)
add_digital("SystemError",                                                    PV_NAME="Sys_Stat",         PV_DESC="System Diagnostics Status",       PV_ONAM="System Error",  PV_ZNAM="System OK")

#################################
#HMI Connection status
add_minor_alarm("HMIConnected",               "HMI Disconnected",             PV_NAME="HMI_ConnStat",     PV_DESC="HMI Connection",                  PV_ONAM="Connected")

#################################
#CPU module
add_major_alarm("Process PLC not connected",  "Process PLC Disconnected",     PV_NAME="CPU_ConnStat",     PV_DESC="Process PLC CPU Connection",      PV_ONAM="Connected")
add_major_alarm("Process PLC status",         "Process PLC Failure",          PV_NAME="CPU_Stat",         PV_DESC="Process PLC CPU Status",          PV_ONAM="OK")

#-#################################
#-Scalance Switch
#-add_major_alarm("Switch not connected",       "Switch Disconnected",          PV_NAME="SCL_ConnStat",     PV_DESC="Scalance Switch Connection",      PV_ONAM="Connected")
#-add_major_alarm("Switch status",              "Switch Failure",               PV_NAME="SCL_Stat",         PV_DESC="Scalance Switch Status",          PV_ONAM="OK")

#################################
#RIO interface module
add_major_alarm("IM155 not connected",        "IM155 Disconnected",           PV_NAME="RIO_IM_ConnStat",  PV_DESC="PN Interface Module Connection",  PV_ONAM="Connected")
add_major_alarm("IM155 status",               "IM155 Failure",                PV_NAME="RIO_IM_Stat",      PV_DESC="PN Interface Module Status",      PV_ONAM="OK")

#################################
#F-DI modules
add_minor_alarm("Slot 1 not connected",       "FDI1 Disconnected",            PV_NAME="FDI1_ConnStat",    PV_DESC="RIO FDI1 Module Connection",      PV_ONAM="Connected")
add_minor_alarm("Slot 2 not connected",       "FDI2 Disconnected",            PV_NAME="FDI2_ConnStat",    PV_DESC="RIO FDI2 Module Connection",      PV_ONAM="Connected")
add_minor_alarm("Slot 3 not connected",       "FDI3 Disconnected",            PV_NAME="FDI3_ConnStat",    PV_DESC="RIO FDI3 Module Connection",      PV_ONAM="Connected")
add_minor_alarm("Slot 4 not connected",       "FDI4 Disconnected",            PV_NAME="FDI4_ConnStat",    PV_DESC="RIO FDI4 Module Connection",      PV_ONAM="Connected")
add_minor_alarm("Slot 5 not connected",       "FDI5 Disconnected",            PV_NAME="FDI5_ConnStat",    PV_DESC="RIO FDI5 Module Connection",      PV_ONAM="Connected")
add_minor_alarm("Slot 6 not connected",       "FDI6 Disconnected",            PV_NAME="FDI6_ConnStat",    PV_DESC="RIO FDI6 Module Connection",      PV_ONAM="Connected")
add_minor_alarm("Slot 7 not connected",       "FDI7 Disconnected",            PV_NAME="FDI7_ConnStat",    PV_DESC="RIO FDI7 Module Connection",      PV_ONAM="Connected")
add_minor_alarm("Slot 8 not connected",       "FDI8 Disconnected",            PV_NAME="FDI8_ConnStat",    PV_DESC="RIO FDI8 Module Connection",      PV_ONAM="Connected")
add_minor_alarm("Slot 1 status",              "FDI1 Failure",                 PV_NAME="FDI1_Stat",        PV_DESC="RIO FDI1 Module Status",          PV_ONAM="OK")
add_minor_alarm("Slot 2 status",              "FDI2 Failure",                 PV_NAME="FDI2_Stat",        PV_DESC="RIO FDI2 Module Status",          PV_ONAM="OK")
add_minor_alarm("Slot 3 status",              "FDI3 Failure",                 PV_NAME="FDI3_Stat",        PV_DESC="RIO FDI3 Module Status",          PV_ONAM="OK")
add_minor_alarm("Slot 4 status",              "FDI4 Failure",                 PV_NAME="FDI4_Stat",        PV_DESC="RIO FDI4 Module Status",          PV_ONAM="OK")
add_minor_alarm("Slot 5 status",              "FDI5 Failure",                 PV_NAME="FDI5_Stat",        PV_DESC="RIO FDI5 Module Status",          PV_ONAM="OK")
add_minor_alarm("Slot 6 status",              "FDI6 Failure",                 PV_NAME="FDI6_Stat",        PV_DESC="RIO FDI6 Module Status",          PV_ONAM="OK")
add_minor_alarm("Slot 7 status",              "FDI7 Failure",                 PV_NAME="FDI7_Stat",        PV_DESC="RIO FDI7 Module Status",          PV_ONAM="OK")
add_minor_alarm("Slot 8 status",              "FDI8 Failure",                 PV_NAME="FDI8_Stat",        PV_DESC="RIO FDI8 Module Status",          PV_ONAM="OK")

#################################
#DI modules
add_minor_alarm("Slot 9 not connected",       "DI1 Disconnected",             PV_NAME="DI1_ConnStat",     PV_DESC="RIO DI1 Module Connection",       PV_ONAM="Connected")
add_minor_alarm("Slot 10 not connected",      "DI2 Disconnected",             PV_NAME="DI2_ConnStat",     PV_DESC="RIO DI2 Module Connection",       PV_ONAM="Connected")
add_minor_alarm("Slot 11 not connected",      "DI3 Disconnected",             PV_NAME="DI3_ConnStat",     PV_DESC="RIO DI3 Module Connection",       PV_ONAM="Connected")
add_minor_alarm("Slot 9 status",              "DI1 Failure",                  PV_NAME="DI1_Stat",         PV_DESC="RIO DI1 Module Status",           PV_ONAM="OK")
add_minor_alarm("Slot 10 status",             "DI2 Failure",                  PV_NAME="DI2_Stat",         PV_DESC="RIO DI2 Module Status",           PV_ONAM="OK")
add_minor_alarm("Slot 11 status",             "DI3 Failure",                  PV_NAME="DI3_Stat",         PV_DESC="RIO DI3 Module Status",           PV_ONAM="OK")

#################################
#AI modules
add_minor_alarm("Slot 12 not connected",      "AI1 Disconnected",             PV_NAME="AI1_ConnStat",     PV_DESC="RIO AI1 Module Connection",       PV_ONAM="Connected")
add_minor_alarm("Slot 12 status",             "AI1 Failure",                  PV_NAME="AI1_Stat",         PV_DESC="RIO AI1 Module Status",           PV_ONAM="OK")

#################################
#F-DQ modules
add_minor_alarm("Slot 13 not connected",      "FDQ1 Disconnected",            PV_NAME="FDQ1_ConnStat",    PV_DESC="RIO FDQ1 Module Connection",      PV_ONAM="Connected")
add_minor_alarm("Slot 14 not connected",      "FDQ2 Disconnected",            PV_NAME="FDQ2_ConnStat",    PV_DESC="RIO FDQ2 Module Connection",      PV_ONAM="Connected")
add_minor_alarm("Slot 15 not connected",      "FDQ3 Disconnected",            PV_NAME="FDQ3_ConnStat",    PV_DESC="RIO FDQ3 Module Connection",      PV_ONAM="Connected")
add_minor_alarm("Slot 13 status",             "FDQ1 Failure",                 PV_NAME="FDQ1_Stat",        PV_DESC="RIO FDQ1 Module Status",          PV_ONAM="OK")
add_minor_alarm("Slot 14 status",             "FDQ2 Failure",                 PV_NAME="FDQ2_Stat",        PV_DESC="RIO FDQ2 Module Status",          PV_ONAM="OK")
add_minor_alarm("Slot 15 status",             "FDQ3 Failure",                 PV_NAME="FDQ3_Stat",        PV_DESC="RIO FDQ3 Module Status",          PV_ONAM="OK")

#################################
#DQ modules
add_minor_alarm("Slot 16 not connected",      "DQ1 Disconnected",             PV_NAME="DQ1_ConnStat",     PV_DESC="RIO DQ1 Module Connection",       PV_ONAM="Connected")
add_minor_alarm("Slot 17 not connected",      "DQ2 Disconnected",             PV_NAME="DQ2_ConnStat",     PV_DESC="RIO DQ2 Module Connection",       PV_ONAM="Connected")
add_minor_alarm("Slot 18 not connected",      "DQ3 Disconnected",             PV_NAME="DQ3_ConnStat",     PV_DESC="RIO DQ3 Module Connection",       PV_ONAM="Connected")
add_minor_alarm("Slot 16 status",             "DQ1 Failure",                  PV_NAME="DQ1_Stat",         PV_DESC="RIO DQ1 Module Status",           PV_ONAM="OK")
add_minor_alarm("Slot 17 status",             "DQ2 Failure",                  PV_NAME="DQ2_Stat",         PV_DESC="RIO DQ2 Module Status",           PV_ONAM="OK")
add_minor_alarm("Slot 18 status",             "DQ3 Failure",                  PV_NAME="DQ3_Stat",         PV_DESC="RIO DQ3 Module Status",           PV_ONAM="OK")
